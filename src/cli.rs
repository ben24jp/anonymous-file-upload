use structopt::StructOpt;

#[derive(Debug, StructOpt)]
/// structopt to parse cli arguments
pub struct Cli {
    #[structopt(short, long)]
    file: String,
}

impl Cli {
    /// gives the path of the file to upload
    pub fn get_filename() -> String {
        let args = Cli::from_args();
        args.file
    }
}
