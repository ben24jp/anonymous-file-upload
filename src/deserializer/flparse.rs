use serde::Deserialize;
use std::borrow::Cow; //must find a way to use cow

// The deserializer for json output
#[derive(Debug, Deserialize)]
/// struct to store the response data from fileleaks.com
pub struct RspData {
    /// status of the post request set to fileleaks.com
    status: bool,
    /// data regarding the file upload to fileleaks.com
    data: Data,
}

#[derive(Debug, Deserialize)]
/// data regarding the file upload to fileleaks.com
struct Data {
    /// uploaded file details returned from fileleaks.com
    file: File,
}

#[derive(Debug, Deserialize)]
/// uploaded file details returned from fileleaks.com
struct File {
    /// urls to the files uploaded to fileleaks.com
    url: Url,
    /// contains the metadata of the file uploaded to fileleaks.com
    metadata: Metadata,
}

#[derive(Debug, Deserialize)]
/// urls to the files uploaded to fileleaks.com
struct Url {
    /// full sized url to the file uploaded to fileleaks.com
    full: String,
    /// shortened url to the file uploaded to fileleaks.com
    short: String,
}

#[derive(Debug, Deserialize)]
/// contains the metadata of the file uploaded to fileleaks.com
struct Metadata {
    /// id of file uploaded to fileleaks.com
    id: String,
    /// name of the file uploaded to fileleaks.com
    name: String,
    /// size if ormation of the file uploaded to fileleaks.com
    size: Size,
}

#[derive(Debug, Deserialize)]
/// size if ormation of the file uploaded to fileleaks.com
struct Size {
    /// size information of file uploaded to fileleaks.com as number
    bytes: u64,
    /// size information of file uploaded to fileleaks.com as string
    readable: String,
}

impl RspData {
    pub fn get_url<'a>(&self) -> Cow<'a, str> {
        let d = self.data.file.url.short.clone();
        return Cow::Owned(d);
    }
}
