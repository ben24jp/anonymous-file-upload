use serde::Deserialize;
use std::borrow::Cow;

#[derive(Debug, Deserialize)]
pub struct RspData {
    status: String,
    data: Data
}

#[derive(Debug, Deserialize)]
#[serde(rename_all="camelCase")]
struct Data {
    guest_token: String,
    download_page: String,
    code: String,
    parent_folder: String,
    file_id: String,
    file_name: String,
    md5: String,
    direct_link: String,
    info: String
}

impl RspData {
    pub fn get_url<'a>(&self) -> Cow<'a, str> {
        let url_clone = self.data.download_page.clone();
        return Cow::Owned(url_clone);
    }
}
