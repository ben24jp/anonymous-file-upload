mod flparse;
mod gfparse;

pub use flparse::RspData as FlData;
pub use gfparse::RspData as GfData;