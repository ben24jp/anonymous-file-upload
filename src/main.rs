use reqwest::blocking::{multipart, Client};
// use serde::Deserialize;
mod cli;
mod deserializer;

use deserializer::GfData;

use cli::Cli;
// use flparse::{RspData};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    upload_file(Cli::get_filename())?;
    Ok(())
}

fn upload_file(path: String) -> Result<(), Box<dyn std::error::Error>> {
    let form = multipart::Form::new().file("file", path)?;
    let client = Client::builder()
        .timeout(None)
        .gzip(true)
        .tcp_nodelay(true)
        .build()?;
    let res = client
        .post("https://store9.gofile.io/uploadFile")
        .multipart(form)
        .send()?;

    let res_json: GfData = res.json()?;
    println!("{:#?}", res_json.get_url());
    Ok(())
}
